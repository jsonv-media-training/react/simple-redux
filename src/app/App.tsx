import React, { FunctionComponent } from 'react';
import './App.scss';
import SongList from "../components/song-list/SongList";
import SongDetail from "../components/song-detail/SongDetail";

const App: FunctionComponent<any> = () => {
  return (
    <div>
      <SongList/>
      <SongDetail/>
    </div>
  );
}

export default App;
