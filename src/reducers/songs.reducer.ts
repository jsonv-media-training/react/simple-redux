import { Song } from "./model";

export const songsReducer = (): Song[] => [
  {
    title: 'No Scrubs',
    duration: '4:05'
  },
  {
    title: 'Macarena',
    duration: '2:30'
  },
  {
    title: 'All Star',
    duration: '3:15'
  },
  {
    title: 'I want it That Way',
    duration: '5:05'
  }
]
