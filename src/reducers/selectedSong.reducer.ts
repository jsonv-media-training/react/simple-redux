import { ActionType } from '../actions/model';
import { Song } from './model';

export const selectedSongReducer = (selectedSong: Song | null = null, action: { type: string, payload: Song}): Song | null => {
  if (action.type === ActionType.SONG_SELECTED_ACTION) {
    return action.payload;
  }

  return selectedSong;
}