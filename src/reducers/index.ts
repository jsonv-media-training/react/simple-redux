import { combineReducers } from 'redux';
import { songsReducer } from "./songs.reducer";
import { selectedSongReducer } from "./selectedSong.reducer";

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
});