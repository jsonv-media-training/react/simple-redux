import { Song } from "../reducers/model";
import { Action, ActionType } from "./model";

export const selectSong = (song: Song): Action<Song> => ({
  type: ActionType.SONG_SELECTED_ACTION,
  payload: song
});

