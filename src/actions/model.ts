export enum ActionType {
  SONG_SELECTED_ACTION = 'SONG_SELECTED'
}

export interface Action<T> {
  type: ActionType,
  payload: T
}