import React, { FunctionComponent, useEffect } from 'react';
import { connect } from 'react-redux';

import './SongList.scss';
import { Song } from "../../reducers/model";
import { selectSong } from "../../actions/selectSong.action";

const SongList: FunctionComponent<{ songs: Song[], selectSong: Function }> = ({ songs, selectSong }) => {

  return (
    <ul className="list-group">
      {
        songs.map(song => {
          return (
            <li key={song.title} className="list-group-item">
              <h5 className="fw-bold">{song.title}</h5>
              <button className="btn btn-primary" onClick={() => selectSong(song)}>Select</button>
            </li>
          )
        })
      }
    </ul>
  )
}

export default connect(
  (state: any) => ({ songs: state.songs }),
  { selectSong }
)(SongList);