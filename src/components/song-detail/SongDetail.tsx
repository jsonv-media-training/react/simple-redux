import React, { FunctionComponent } from 'react';
import { connect } from 'react-redux';

import '../song-list/SongList.scss';
import { Song } from "../../reducers/model";

const SongDetail: FunctionComponent<{ selectedSong: Song }> = ({ selectedSong }) => {

  return (
    <div className="card d-flex justify-content-center align-items-center" style={{ height: '100px' }}>
      <div className="card-title h2">{ selectedSong ? selectedSong.title : 'Please select a song!'}</div>
      { selectedSong && <h4>{selectedSong.duration}</h4> }
    </div>
  )
}

export default connect(
  (state: any) => ({ selectedSong: state.selectedSong }),
)(SongDetail);